#from get_config import Config
#from sql import Query
from content import HTMLtable
from plot import MyPlot
from multiprocessing import Process
#from send_table import Email
from datetime import datetime, timedelta

#config = Config('source/config.ini')
#headers = config.get_obj("tables", "headers")
#name_label = config.get_obj("tables", "name_label")
headers = ['START_TIME', 'END_TIME', 'DURATION']
table_name = 'Test_table'

rows = [['2021-03-05 09:56:58.490', '2021-03-04 10:08:58.300', '34.7', 'OK', '1'],
        ['2021-03-04 07:56:58.490', '2021-03-04 10:08:58.300', '21.7', 'OK', '2'],
        ['2021-03-03 07:58:12.460', '2021-03-03 11:53:24.100', '280', 'OK', '1'],
        ['2021-03-02 11:32:26.010', '2021-03-02 12:25:46.620', '22.7', 'OK', '3'],
        ['2021-03-01 07:32:26.010', '2021-03-01 08:25:46.620', '14.7', 'OK', '1'],
        ['2021-03-01 08:34:26.010', '2021-03-01 08:52:46.620', '0', 'FAIL', '0'],
        ['2021-02-28 06:36:26.010', '2021-03-02 06:42:46.620', '61.7', 'OK', '1'],
        ['2021-02-28 06:06:26.010', '2021-03-02 06:12:46.620', '14.6', 'OK', '2']]

for row in range(len(rows)):
    for element in range(len(rows[row])):
        if element in [0, 1]:
            rows[row][element] = datetime.strptime(rows[row][element], '%Y-%m-%d %H:%M:%S.%f')
        else:
            #print('Smth is wrong with translating the values\n', rows[row][element] )
            pass


def action(st):
    #rows = Query('source/query/' + name_label[st]["file"] + '.sql').exec_query().get_rows()
    #image = MyPlot(st, rows, 'source/images/' + name_label[st]["file"] + '.png').draw_plot()
    #table = HTMLtable('source/tables/' + name_label[st]["file"] + '.csv', rows, headers,
    #                  name_label[st]["label"]).create_table()
    MyPlot(rows=rows, filename='/Users/a18975965/Desktop/stuff/testfig1.png', start=6, end=9, title='test_title').draw_plot()
    table = HTMLtable('/Users/a18975965/Desktop/stuff/testtable.csv', rows, headers, table_name).create_table()

process = []
action(1)
#for st in name_label:
#    process.append(Process(target=action, args=(st,)))
#    process[-1].start()

#for key in process:
#    key.join()

#message = Email('source/email/addresses.ini')
#message.generate_msg('source/config.ini')
#message.send_email()