import numpy as np
from datetime import datetime, timedelta

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import matplotlib.ticker as ticker


class MyPlot:

    def __init__(self, rows, im_file_name, start, end, title):
        self.rows = rows
        self.im_file_name = im_file_name
        self.start = start
        self.end = end
        self.title = title
        self.duration = []
        self.duration_ok = []
        self.duration_no = []
        self.duration_
        self.x_axis = []
        self.times = self.make_time_array(self.start, self.end)
        self.board_TLA = (self.end - self.start) * 60
        self.board = 60
        self.yticks = list(range(0, (self.end - self.start) * 60 + 30, 30))

    def make_time_array(self, start, end):
        ticks = np.arange(start, end + 0.5, 0.5)
        sample_time = datetime(2014, 2, 3, 0, 0, 0)
        self.times = [(str((sample_time + timedelta(0, 60 * x)).time()))[3:] for x in ticks]
        return self.times

    def set_grid(self):
        self.ax.grid(which='major', color='#555555', linewidth = 0.5)
        self.ax.grid(which='minor', color='#555555', linewidth = 0.5)

    def create_base(self):
        self.fig = plt.figure()
        self.fig.patch.set_facecolor('#303030')
        self.fig.set_figheight(5)
        self.fig.set_figwidth(8)
        self.ax = self.fig.add_subplot(111)
        self.ax.patch.set_facecolor('#303030')
        self.set_ax_style()
        self.set_ticks_style()
        self.set_grid()

    def set_ticks_style(self):
        self.ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
        self.ax.minorticks_on()
        self.ax.set_yticks(self.yticks)
        self.ax.set_yticklabels(self.times)
        self.ax.tick_params(which='both', axis='x', colors='#BFBFBF', labelrotation=45)
        self.ax.tick_params(which='both', axis='y', colors='#BFBFBF')

    def set_ax_style(self):
        self.ax.xaxis.set_major_formatter(mdates.DateFormatter("%d.%m"))
        #self.ax.yaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
        self.ax.set_title(self.title, loc='center', pad=16, color='#BFBFBF')
        self.ax.spines['bottom'].set_color('#BFBFBF')
        self.ax.spines['top'].set_color('#BFBFBF')
        self.ax.spines['left'].set_color('#BFBFBF')
        self.ax.spines['right'].set_color('#BFBFBF')


    def draw_plot(self):
        self.duration_ok = [0] * len(self.rows)
        self.duration_no = [0] * len(self.rows)
        for i in range(len(self.rows)):
            if float(self.rows[i][2]) >= self.board_TLA:
                self.rows[i][2] = self.board_TLA
            self.duration.append(float(self.rows[i][2]))
            #!!!!!!!!!!!!!self.x_axis.append((datetime.strptime(self.rows[i][0], '%Y-%m-%d %H:%M:%S.%f')))
            self.x_axis.append(self.rows[i][0])
            if float(self.rows[i][2]) >= self.board:
                self.duration_no[i] = float(self.rows[i][2])
            else:
                self.duration_ok[i] = float(self.rows[i][2])

        self.x_axis = [round(date2num(x)) for x in self.x_axis]
        self.create_base()
        bar_offset = 0.1
        x_axis_ok = [x + bar_offset for x in self.x_axis]
        x_axis_no = [x - bar_offset for x in self.x_axis]
        self.ax.bar(x_axis_ok, self.duration_ok, width=0.3, color='#11b862')
        self.ax.bar(x_axis_no, self.duration_no, width=0.3, color='#de425b')
        self.ax.axhline(y=self.board, xmin=0, xmax=1, ls=':', color='#f9e282', label='TLA')
        self.set_ticks_style()
        self.set_ax_style()
        self.ax.legend(facecolor='#525050', labelcolor='#BFBFBF')
        plt.savefig(self.im_file_name)
        plt.show()









