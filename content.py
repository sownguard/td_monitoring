import datetime
#from send_table import Email


class HTMLtable:

    def __init__(self, file, in_rows=[], table_headers='', table_label=''):
        self.color_str = {0: """<tr style="background-color: rgb(220,230,241);">""", 1: """<tr>"""}
        self.file = file
        self.rows = in_rows
        self.headers = table_headers
        self.label = table_label
        self.table = [
            self.label + """</br><table style="line-height: 10.0px; border: 2px solid rgb(149,179,215); border-collapse: collapse;">""",
            """<tr style="color: rgb(255,255,255);background-color: rgb(79,129,189);">""" +
            ''.join(map(self.table_cell, self.headers)) + """</tr>"""]

    def table_cell(self, st):
        """
        Оборачиваем строку в HTML тег
        :param st: строка
        :return: полученая строка
        """
        if isinstance(st, datetime.date):
            st = st.strftime("%d.%m.%Y")
        if isinstance(st, datetime.time):
            st = st.strftime("%H:%M")
        if st is None:
            st = ''
        return """<td style="border-color: rgb(149,179,215); padding-left: 2px; padding-right: 3px; border: 1px solid rgb(149,179,215);">""" + str(
            st).strip() + """</td>"""

    def create_table(self):
        i = 0
        for st in self.rows:
            self.table.append(self.color_str[i % 2] + ''.join(map(self.table_cell, st[:-1])) + """</tr>""")
            i += 1
        self.table.append("""</table>""")
        self.save()

    def save(self):
        with open(self.file, "w", -1, "utf8") as fp:
            try:
                for st in self.table:
                    fp.write(st + '\n')
            except Exception as e:
                print('Exception in writing a file with a result table')
                #Email.error_send(u'ERROR Мониторинг МИС', str(e))
